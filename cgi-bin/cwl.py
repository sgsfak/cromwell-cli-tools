#!/usr/local/bin/python3
"""This is a slack slash command implementation for retrieving
the status of a workflow run from Cromwell...

It is implemented through some ancient technology, called CGI :-)
Returns a JSON formatted response, as described in
    https://api.slack.com/interactivity/slash-commands
"""

import json
import requests
import os
import urllib

def get_meta(runid):
    url = os.environ('CROMWELL_SERVER') + '/api/workflows/v1/' + runid + '/metadata'
    r = requests.get(url)
    r.raise_for_status()
    js = json.loads(r.content)
    status = js['status']
    d = []
    for k,v in js['calls'].items():
        step = k
        data = v[0]
        st = {'step': step, 'status': data['executionStatus'], 'start': data['start'], 'end': data['end']}
        d.append(st)
    return {'status':status, 'steps': d}

def format_progress(status):
    l = ["{:>30}\t{}\t{}\t{}\n".format(st['step'], st['status'], st['start'], st['end']) for st in status['steps']]
    s = "The status of run `{}` is `{}`\n\nProgress:\n```\n{}```".format(runid, status['status'], "".join(l))
    return s

if __name__ == "__main__":
    qs = urllib.parse.parse_qs(os.environ['QUERY_STRING'])
    runid = qs['text'][0]
    status = get_meta(runid)
    progress_md = format_progress(status)
    js = {"response_type": "in_channel",
          "text": progress_md}
    print("Content-type: application/json\n\n")
    print(json.dumps(js))
