# cromwell-cli-tools

These are some simple command line scripts for accessing the [Cromwell Server REST API](https://cromwell.readthedocs.io/en/develop/api/RESTAPI/). They are actually thin wrappers around `curl` and [jq](https://stedolan.github.io/jq/), which is used for parsing the JSON responses. We assume a Unix-like operating system, and more specifically I have tested them in Linux and MacOS. There is also a Dockerfile to build a lightweight Docker image (based on [Alpine Linux](https://hub.docker.com/_/alpine)) and to use it like this:

```bash
docker build -t cromwell-cli .
docker run -t cromwell-cli getStatus.sh 89c5e21f-a278-4219-b8fc-eefeb80464ca
```

## Usage

The scripts require the URL for contacting the Cromwell server and have already a test URL hardcoded in them. The user can define the `CROMWELL_SERVER` environment variable if s/he needs to change the URL used, e.g. 

```bash
export CROMWELL_SERVER=http://cromwell.example.com
```

Or, using the [`-e` option](https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file) in the case of running through docker:

```bash
docker run -e CROMWELL_SERVER=http://cromwell.example.com -t cromwell-cli getStatus.sh 89c5e21f-a278-4219-b8fc-eefeb80464ca
```

### Submitting a workflow for execution : `submit.sh`

Use the `submit.sh` supplying the URL for the workflow and the path for the file containing the inputs to the workflow to run, as follows:

```bash
./submit.sh <workflow-url> <workflow-inputs-file>
```

The script then prints the `id` of the run.

**NOTE**: The script assumes that the workflow uses the CWL syntax (i.e. not WDL)

### Submitting a workflow for execution "by source": `submitSrc.sh`

`submitSrc.sh` can be used to submit a local workflow file (instead of making available through a URL). This can be usefull when making local changes to a workflow before uploading it to a central repository. 

**_Caveats_**: __Obviously, this will not work properly if the workflow includes references to tools or other workflows using relative paths.__

The syntax is the following:

```bash
./submitSrc.sh <workflow-source-file> <workflow-inputs-file>
```

Again, it prints the `id` of the workflow execution.

### Getting the status of a workflow run: `getStatus.sh`

You need to supply the `id` of a run and it prints its status in standard output:

```bash
getStatus.sh ea9a6cf4-8199-4e98-9b7f-2ce446d20189
Failed  Unable to execute HTTP request: Read timed out
```

For a periodic check of the execution we can use the [`watch` command](https://linux.die.net/man/1/watch) as follows to run `getStatus.sh` periodically:

```bash
# Run getStatus.sh every 10 seconds:
watch -n 10 ./getStatus.sh ea9a6cf4-8199-4e98-9b7f-2ce446d20189
```

### Get outputs of a successful run: `getOutputs.sh`

You just need to supply the run `id` 

```bash
getOutputs.sh <run-id>
```

### Get inputs of a run: `getInputs.sh`

You just need to supply the run `id` 

```bash
getInputs.sh <run-id>
```

### Get the error for a failed run: `getError.sh`

```bash
getError.sh <run-id>
```


### Get the status of all the steps in a workflow: `getProgress.sh`

```bash
getProgress.sh <run-id>
```

Returns a formatted table where each row contains the name of the step, 
its running status, the start and end timestamp, e.g.

```
> getProgress.sh 3f5b7a5f-6d88-448c-8a90-9da3d191bd10
tmap                    Done     2019-08-07T15:26:56.695Z  2019-08-07T15:38:29.323Z
samtools_sort           Done     2019-08-07T15:38:30.607Z  2019-08-07T15:41:47.338Z
samtools_index          Done     2019-08-07T15:41:48.557Z  2019-08-07T15:45:16.373Z
coverage_analysis       Running  2019-08-07T15:45:18.017Z
torrent_variant_caller  Running  2019-08-07T15:45:18.017Z
```

### Show status of the run in a graphical way in the browser

The following will open your browser and show the status of the run using progress bars:
```bash
showStatus.sh <run-id>
```

Example screenshot:

![progress status](img/status.png)

### Get all the available information (metadata) for a run: `getMeta.sh`

```bash
getMeta.sh <run-id>
```
