#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "Usage: `basename $0` <run-id>"
    exit 1
fi

ID=$1
SERVER=${CROMWELL_SERVER:-http://cromwell.testbed-precmed.iit.demokritos.gr}
SERVER="${SERVER%%/}"
URL="$SERVER/api/workflows/v1/$ID/timing"

case "$(uname -s)" in
    Darwin)
        OPEN_CMD=open
        ;;
    Linux)
        OPEN_CMD=xdg-open
        ;;
    *)
        echo Open the following URL with your browser:
        echo $URL
        exit
        ;;
esac

$OPEN_CMD "$SERVER/api/workflows/v1/$1/timing"
