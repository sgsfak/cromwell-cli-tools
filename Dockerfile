FROM alpine
ENV TERM xterm
RUN apk add curl jq bash ncurses
WORKDIR /scripts
COPY *.sh /scripts/
ENV PATH=/scripts:$PATH
ENTRYPOINT [ "/bin/bash" ]
